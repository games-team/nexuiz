#!/bin/sh

set -e

SVN=11231
RELEASE=2.5.2
SUFFIX=+dp

rm -rf nexuiz-${RELEASE}${SUFFIX}.orig nexuiz_${RELEASE}${SUFFIX}.orig.tar.gz
mkdir nexuiz-${RELEASE}${SUFFIX}.orig

for f in COPYING nexuiz.ico nexuiz.xpm; do
    svn export -r${SVN} \
        svn://svn.icculus.org/twilight/trunk/darkplaces/$f \
        nexuiz-${RELEASE}${SUFFIX}.orig/$f
done

tar -czvf nexuiz_${RELEASE}${SUFFIX}.orig.tar.gz nexuiz-${RELEASE}${SUFFIX}.orig
rm -rf nexuiz-${RELEASE}${SUFFIX}.orig
